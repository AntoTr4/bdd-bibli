from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, SelectField
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo
from app.models import User, Item

# Définit le formulaire de login
class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')


# Définit le formulaire d'entrée d'un nouvel item dans la BDD
class NewItemForm(FlaskForm):
    title = StringField('Title', validators=[DataRequired()])
    author = StringField('Author', validators=[DataRequired()])
    itemType = SelectField('Item type', validators=[DataRequired()],
                           choices=[('book', 'Book'), ('cd', 'CD'), ('dvd', 'DVD'),
                                    ('magazine', 'Magazine'), ('comic', 'Comic Book')])
    not_borrowable = BooleanField('Not borrowable')
    submit = SubmitField('Add item')

    # Vérifie que l'item n'est pas déjà dans la BDD (par le titre, qui doit être unique)
    def validate_title(self, title):
        query = Item.query.filter_by(title=title.data).first()
        if query is not None:
            raise ValidationError('This item is already in the database.')


# Définit le formulaire de sélection du type d'idem à afficher dans l'index
class TypeSelectForm(FlaskForm):
    itemType = SelectField('Filter by type',
                           choices=[('All','All'), ('book', 'Book'), ('cd', 'CD'), ('dvd', 'DVD'),
                                    ('magazine', 'Magazine'), ('comic', 'Comic Book')])
    submit = SubmitField('Search')


# Définit le formulaire de sélection du type d'utilisateur à afficher dans la page admin
class UserSelectForm(FlaskForm):
    user_status = SelectField('Filter by status',
                           choices=[('All','All'), ('excluded', 'Excluded')])
    submit = SubmitField('Filter')


# Définit le formulaire de création de compte
class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    status = SelectField('Status', validators=[DataRequired()],
                         choices=[('Full price', 'Other'), ('Free', 'Unemployed'), ('Free', 'Student'),
                                  ('Half price', 'Veteran'), ('Half price', 'Senior')])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField(
        'Repeat Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Register')

    # Vérifie que le nom d'utilisateur est libre
    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError('Please use a different username.')

    # Vérifie qu'un compte n'est pas déjà associé à cet email
    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Please use a different email address.')