from datetime import datetime, timedelta
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from app import db, login


# Définit la classe utilisateur
class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    last_seen = db.Column(db.DateTime, default=datetime.utcnow)
    status = db.Column(db.String(64), default='Full price')
    sub_date = db.Column(db.DateTime, default=datetime.utcnow)
    excluded_until = db.Column(db.DateTime, default=None)

    # Attribue un mot de passe crypté à partir de l'entrée de l'utilisateur
    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    # Vérifie que le mot de passe entré correspond au mot de passe crypté
    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    # Renvoie la date de fin de validité de la souscription courante
    def sub_available_until(self):
        return self.sub_date + timedelta(days=365)

    # Vérifie que la souscription courante est encore valable
    def sub_available(self):
        return self.sub_available_until() > datetime.utcnow()

    def __repr__(self):
        return '<User {}>'.format(self.username)



# Définit la classe objet
class Item(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(140))
    author = db.Column(db.String(140))
    itemType = db.Column(db.String(140))
    not_borrowable = db.Column(db.Boolean, default=False)

    # Vérifie que l'item est un livre
    def is_book(self):
        return self.itemType.lower().strip() == "book"

    # Vérifie que l'item est disponible
    def is_available(self):
        return db.session.query(Borrow).filter_by(item=self.id, return_date=None).count() == 0

    def __repr__(self):
        return '<Item {}, {}>'.format(self.id, self.title)



# Définit la classe emprunt
class Borrow(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    item = db.Column(db.Integer, db.ForeignKey('item.id'))
    user = db.Column(db.Integer, db.ForeignKey('user.id'))
    date = db.Column(db.DateTime, default=datetime.utcnow())
    return_date = db.Column(db.DateTime, default=None)
    penalty = db.Column(db.Integer, default=None)

    # Acte le retour de l'objet concerné dans la bibliothèque
    def close(self):
        self.return_date = datetime.utcnow()

    # Retourne la durée maximale d'un emprunt
    def max_borrow_time(self):
        return timedelta(days=30)

    # Renvoie le nombre de jour de retard de rendu d'un emprunt (pas en retard si négatif)
    def days_late(self):
        return (datetime.utcnow() - (self.date + self.max_borrow_time())).days

    # Calcule la pénalité associée à un emprunt, si elle est justifiée
    def compute_penalty(self):
        if self.days_late() > 3 and self.return_date == None:
            self.penalty = self.days_late()
            db.session.commit()

    def __repr__(self):
        return '<Borrow {} : item {} by user {}>'.format(self.id, self.item, self.user)


@login.user_loader
def load_user(id):
    return User.query.get(int(id))
