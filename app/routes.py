from flask import render_template, flash, redirect, url_for, request
from app import app
from app import db
from app.models import User, Item, Borrow
from sqlalchemy import join
from flask_login import current_user, login_user, logout_user, login_required
from app.forms import LoginForm, RegistrationForm, NewItemForm, TypeSelectForm, UserSelectForm
from datetime import datetime, timedelta


# MàJ de la date de dernière visite
@app.before_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()

# Route vers la page d'index
@app.route('/index', methods=['GET', 'POST'])
def index():
    itemList = []
    authorList = []
    type = 'All'

    # Gestion du select sur le type d'item
    form = TypeSelectForm()
    if form.validate_on_submit():
        type = form.itemType.data
    if type=='All':
        items = Item.query.all()
    else:
        items = Item.query.filter_by(itemType=type)

    # Gestion des données à envoyer à la barre de recherche JS
    for item in Item.query.all():
        itemList.append(item.title)
        # Inclure la recherche par nom d'auteur
        if not item.author in authorList:
            authorList.append(item.author)

    if request.method == 'POST':
        # Gestion de l'emprunt d'un item
        if request.form.get("borrow") == 'Borrow':
            item = Item.query.filter_by(id=request.form.get('id')).first()
            borrowed_items = db.session.query(Item).join(Borrow).filter_by(user=current_user.id, return_date=None)

            if item.is_book():
                nbBorrowedBooks = borrowed_items.filter(Item.itemType == "book").count()
                if nbBorrowedBooks >= 3:
                    flash("You can't borrow more than 3 books. Please return one before you borrow another.","danger")
                else:
                    borrow = Borrow(user=current_user.id, item=item.id)
                    db.session.add(borrow)
                    db.session.commit()
                    flash(item.title + ' has been added to your borrowed ' + item.itemType + 's !', 'info')
            else:
                nbBorrowedOthers = borrowed_items.filter(Item.itemType != "book").count()
                if nbBorrowedOthers >= 2:
                    flash("You can't borrow more than 2 CDs, DVDs, magazines or comic books."
                          "Please return one before you borrow another.", "danger")
                else:
                    borrow = Borrow(user=current_user.id, item=item.id)
                    db.session.add(borrow)
                    db.session.commit()
                    flash(item.title + ' has been added to your borrowed ' + item.itemType + 's !', 'info')
    return render_template('index.html', title='Home', items=items, itemList=itemList, authorList=authorList,
                           form=form, type=type)


# Route vers la page de login
@app.route('/', methods=['GET', 'POST'])
@app.route('/login', methods=['GET', 'POST'])
def login():
    # Redirige vers la page d'accueil si l'utilisateur est déjà loggé
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    # Gestion du formulaire de login
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password','danger')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        if current_user.username == "admin":
            return redirect(url_for('admin'))
        return redirect(url_for('user', username=current_user.username))
    return render_template('login.html', title='Sign In', form=form)


# Route vers la page de logout
@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('login'))


# Route vers la page de création de compte
@app.route('/register', methods=['GET', 'POST'])
def register():
    # Redirige vers la page d'accueil si l'utilisateur est déjà loggé
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    # Gestion du formulaire de register
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data, status=form.status.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!','info')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)


# Route vers la page profil utilisateur
@app.route('/user/<username>', methods=['GET', 'POST'])
@login_required
def user(username):
    # Sécurité si l'admin accède à cette page par l'url
    if current_user.username == "admin":
        return redirect(url_for('admin'))
    # Requêtes pour les données utilisateur
    user = User.query.filter_by(username=username).first_or_404()
    borrows = db.session.query(Borrow).filter_by(user=current_user.id, return_date=None).all()
    borrowed_items = []
    total_penalty = 0

    # Ré-habilite l'utilisateur exclut si le délai est dépassé
    if current_user.excluded_until and datetime.utcnow() > current_user.excluded_until:
        current_user.excluded_until = None
        db.session.commit()

    # Calcul des pénalités
    for borrow in borrows:
        borrow.compute_penalty()
        item = db.session.query(Item).filter_by(id=borrow.item).first()
        borrowed_items.append({'borrow': borrow, 'item': item})
        if borrow.penalty:
            total_penalty+=borrow.penalty

    nb_penalties = db.session.query(Borrow).filter_by(user=current_user.id)\
        .filter(Borrow.date > current_user.sub_date, Borrow.penalty > 3).count()

    # Gestion du cas d'exclusion
    if nb_penalties >= 3:
        current_user.excluded_until = datetime.utcnow() + timedelta(days=730)
        db.session.commit()

    if request.method == 'POST':
        # Gestion de la méthode de retour d'item
        if request.form.get("return") == 'Return':
            item = Item.query.filter_by(id=request.form.get('id')).first()

            borrow_to_close = db.session.query(Borrow).filter_by(return_date=None, item=item.id).first()
            borrow_to_close.close()

            db.session.add(borrow_to_close)
            db.session.commit()
            flash(item.title + ' has been returned to the library !', 'info')
            return redirect(url_for('user', username=current_user.username))
        # Gestion de la souscription
        if request.form.get("subscribe") == 'Subscribe':
            current_user.sub_date = datetime.utcnow()
            db.session.commit()
    return render_template('user.html', user=user, borrowed_items=borrowed_items, total_penalty=total_penalty)


# Route vers la page d'ajout de nouvelles entrées dans la BDD
@app.route('/add_ref', methods=['GET', 'POST'])
@login_required
def add_ref():
    # Page réservée au compte admin
    if not current_user.username == "admin":
        return redirect(url_for('user', username=current_user.username))
    # Gestion du formulaire d'ajout
    form = NewItemForm()
    if form.validate_on_submit():
        item = Item(title=form.title.data, author=form.author.data, itemType=form.itemType.data, not_borrowable=form.not_borrowable.data)
        db.session.add(item)
        db.session.commit()
        flash( form.title.data + ' has been added successfully !', 'info')
    return render_template('add_ref.html', title='Add reference', form=form)


# Route vers la page d'administration
@app.route('/admin', methods=['GET', 'POST'])
@login_required
def admin():
    # Page réservée au compte admin
    if not current_user.username == "admin":
        return redirect(url_for('user', username=current_user.username))
    users = db.session.query(User).filter(User.username != "admin").all()

    # Récupération des données user pour la barre de recherche
    userList = []
    for user in User.query.all():
        userList.append(user.username)

    form = UserSelectForm()
    # Affichage des informations utilisateur
    if form.validate_on_submit():
        status = form.user_status.data
        if status=='All':
            users = User.query.filter(User.username != "admin").all()
            for user in users:
                print(user.excluded_until)
        else:
            print('vincent rend')
            users = User.query.filter(User.username != "admin").filter(User.excluded_until != None).all()
    return render_template('admin.html', title='Admin', users=users, userList=userList, form=form)


# Route vers la page informative par utilisateur
@app.route('/user_details/<username>', methods=['GET', 'POST'])
@login_required
def user_details(username):
    # Page réservée au compte admin
    if not current_user.username == "admin":
        return redirect(url_for('user', username=current_user.username))
    if username == "admin":
        return redirect(url_for('admin'))

    # Affichage des informations utilisateur
    user = db.session.query(User).filter_by(username=username).first_or_404()
    borrows = db.session.query(Borrow).filter_by(user=user.id, return_date=None).all()
    borrowed_items = []
    total_penalty = 0

    # Ré-habilite l'utilisateur exclut si le délai est dépassé
    if user.excluded_until and datetime.utcnow() > user.excluded_until:
        user.excluded_until = None
        db.session.commit()

    # Calcul des pénalités
    for borrow in borrows:
        borrow.compute_penalty()
        item = db.session.query(Item).filter_by(id=borrow.item).first()
        borrowed_items.append({'borrow': borrow, 'item': item})
        if borrow.penalty:
            total_penalty+=borrow.penalty

    # Cas d'exclusion
    nb_penalties = db.session.query(Borrow).filter_by(user=current_user.id)\
        .filter(Borrow.date > user.sub_date, Borrow.penalty > 3).count()
    if nb_penalties >= 3:
        user.excluded_until = datetime.utcnow() + timedelta(days=730)
        db.session.commit()

    # Récupération des informations sur les anciens emprunts
    old_borrows = db.session.query(Borrow).filter_by(user=user.id).filter(Borrow.return_date != None).all()
    old_borrowed_items = []
    for borrow in old_borrows:
        item = db.session.query(Item).filter_by(id=borrow.item).first()
        old_borrowed_items.append({'borrow': borrow, 'item': item})
    return render_template('user_details.html', title='User details', user=user,
                           borrowed_items=borrowed_items, old_borrowed_items=old_borrowed_items,
                           total_penalty=total_penalty)


# Route vers la page informative par item
@app.route('/item/<title>', methods=['GET', 'POST'])
def item(title):
    item = db.session.query(Item).filter_by(title=title).first_or_404()
    if request.method == 'POST':
        # Gestion de l'emprunt
        if request.form.get("borrow") == 'Borrow':
            item = Item.query.filter_by(id=request.form.get('id')).first()
            borrowed_items = db.session.query(Item).join(Borrow).filter_by(user=current_user.id, return_date=None)

            if item.is_book():
                nbBorrowedBooks = borrowed_items.filter(Item.itemType == "book").count()
                if nbBorrowedBooks >= 3:
                    flash("You can't borrow more than 3 books. Please return one before you borrow another.", "danger")
                else:
                    borrow = Borrow(user=current_user.id, item=item.id)
                    db.session.add(borrow)
                    db.session.commit()
                    flash(item.title + ' has been added to your borrowed ' + item.itemType + 's !', 'info')
            else:
                nbBorrowedOthers = borrowed_items.filter(Item.itemType != "book").count()
                if nbBorrowedOthers >= 2:
                    flash("You can't borrow more than 2 CDs, DVDs, magazines or comic books."
                          "Please return one before you borrow another.", "danger")
                else:
                    borrow = Borrow(user=current_user.id, item=item.id)
                    db.session.add(borrow)
                    db.session.commit()
                    flash(item.title + ' has been added to your borrowed ' + item.itemType + 's !', 'info')
        # Gestion du retour
        elif request.form.get("return") == 'Return':
            item = Item.query.filter_by(id=request.form.get('id')).first()

            borrow_to_close = db.session.query(Borrow).filter_by(return_date=None, item=item.id).first()
            borrow_to_close.close()

            db.session.add(borrow_to_close)
            db.session.commit()
            flash(item.title + ' has been returned to the library !','info')
    # Récupération des informations de l'emprunteur, si l'objet est indisponible
    if not item.is_available():
        item_borrow = Borrow.query.filter_by(item=item.id, return_date=None).first()
        item_borrower = User.query.filter_by(id=item_borrow.user).first()
    else:
        item_borrow, item_borrower = None, None
    return render_template('item.html', item=item, item_borrow=item_borrow, item_borrower=item_borrower)


# Route vers la page informative par auteur
@app.route('/author/<author>', methods=['GET', 'POST'])
def author(author):
    items = Item.query.filter_by(author=author).all()
    if request.method == 'POST':
        # Gestion de l'emprunt
        if request.form.get("borrow") == 'Borrow':
            item = Item.query.filter_by(id=request.form.get('id')).first()
            borrowed_items = db.session.query(Item).join(Borrow).filter_by(user=current_user.id, return_date=None)

            if item.is_book():
                nbBorrowedBooks = borrowed_items.filter(Item.itemType == "book").count()
                if nbBorrowedBooks >= 3:
                    flash("You can't borrow more than 3 books. Please return one before you borrow another.", "danger")
                else:
                    borrow = Borrow(user=current_user.id, item=item.id)
                    db.session.add(borrow)
                    db.session.commit()
                    flash(item.title + ' has been added to your borrowed ' + item.itemType + 's !', 'info')
            else:
                nbBorrowedOthers = borrowed_items.filter(Item.itemType != "book").count()
                if nbBorrowedOthers >= 2:
                    flash(
                        "You can't borrow more than 2 CDs, DVDs, magazines or comic books."
                        "Please return one before you borrow another.")
                else:
                    borrow = Borrow(user=current_user.id, item=item.id)
                    db.session.add(borrow)
                    db.session.commit()
                    flash(item.title + ' has been added to your borrowed ' + item.itemType + 's !', 'info')
        # Gestion du retour
        elif request.form.get("return") == 'Return':
            item = Item.query.filter_by(id=request.form.get('id')).first()

            borrow_to_close = db.session.query(Borrow).filter_by(return_date=None, item=item.id).first()
            borrow_to_close.close()

            db.session.add(borrow_to_close)
            db.session.commit()
            flash(item.title + ' has been returned to the library !','info')
    return render_template('author.html', author=author, items=items)

